use crate::solve_quadratic;
use crate::Dot;
use crate::MyDrawToWindow;
use macroquad::prelude::*;

#[derive(Clone, Copy, PartialEq)]
pub struct Ball {
    pub pos: Dot,
    pub vel: Dot,
    pub r: f32,
    pub m: f32,
}

impl MyDrawToWindow for Ball {
    fn draw(&self) {
        draw_circle(self.pos.x, self.pos.y, self.r, YELLOW);
        draw_line(
            self.pos.x,
            self.pos.y,
            self.pos.x + self.vel.x,
            self.pos.y + self.vel.y,
            self.r * 0.05,
            RED,
        );
    }
}

impl Ball {
    pub fn new(
        position_x: f32,
        position_y: f32,
        velocity_x: f32,
        velocity_y: f32,
        radius: f32,
        mass: f32,
    ) -> Ball {
        Ball {
            pos: Dot {
                x: position_x,
                y: position_y,
            },
            vel: Dot {
                x: velocity_x,
                y: velocity_y,
            },
            r: radius,
            m: mass,
        }
    }

    pub fn collision_resolution_ball(ball1: Ball, ball2: Ball) -> (Ball, Ball) {
        if Dot::dst(ball1.pos, ball2.pos) >= ball1.r + ball2.r {
            return (ball1, ball2);
        }
        let mut b1 = ball1.clone();
        let mut b2 = ball2.clone();

        // Rotate to this phys task:
        //               ___              Y
        //    #####__   | \####           ^
        //  #######/|   ###\#####         |
        // #####/##### #####\#####        |
        //  #########   #########         |
        //    #####       #####           |
        //                                |
        // -------------------------------+-----> X
        //                                |
        // 2 balls with V1 and V2, with m1 and m2
        // collide parallel with X

        let to_ox_solve = 2.0 * std::f32::consts::PI - Dot::angle(b1.pos, b2.pos);

        let v1 = Dot::rotate(b1.pos, b1.pos + b1.vel, to_ox_solve) - b1.pos;
        let v2 = Dot::rotate(b2.pos, b2.pos + b2.vel, to_ox_solve) - b2.pos;
        let m1 = b1.m;
        let m2 = b2.m;

        // Sources:
        //
        // (No F in y)
        // V1.y = V3.y, V2.y = V4.y
        //
        // (Translational momentum)
        // m1V1x + m2V2x = m1V3x + v2V4x
        //
        // (Conservation of energy)
        // m1V1^2 + m2V2^2 = m1(V1.y^2 + V3.x^2) + m1(V2.y^2 + V4.x^2)
        //
        // Solve for V3 and V4:
        let v4 = Dot {
            x: solve_quadratic(
                m2 + m2 * m2 / m1,
                -2.0 * m2 * m2 / m1 * v2.x - 2.0 * m2 * v1.x,
                2.0 * m2 * v1.x * v2.x + m2 * m2 / m1 * v2.x * v2.x - m2 * v2.x * v2.x,
            )
            .0
            .expect("No solution"),
            y: v2.y,
        };

        let v3 = Dot {
            x: v1.x + m2 / m1 * v2.x - m2 / m1 * v4.x,
            y: v1.y,
        };

        b1.vel = Dot::rotate(b1.pos, b1.pos + v3, -to_ox_solve) - b1.pos;
        b2.vel = Dot::rotate(b2.pos, b2.pos + v4, -to_ox_solve) - b2.pos;

        (b1, b2)
    }
    /*
    fn collision_resolution_wall(source_ball: Ball, wall: Line) -> Ball {
        let mut ball = source_ball.clone();
        if Dot::dst_to_line(ball.pos, wall) >= ball.r + wall.r {
            return ball;
        }
        let perp_wall_angle = wall.angle() + 0.5 * std::f32::consts::PI;
        let wall_lines = (
            Line {
                pos1: wall.pos1
                    + Dot {
                        x: wall.r * perp_wall_angle.cos(),
                        y: wall.r * perp_wall_angle.sin(),
                    },
                pos2: wall.pos2
                    + Dot {
                        x: wall.r * perp_wall_angle.cos(),
                        y: wall.r * perp_wall_angle.sin(),
                    },
                r: 0.0,
            },
            Line {
                pos1: wall.pos1
                    - Dot {
                        x: wall.r * perp_wall_angle.cos(),
                        y: wall.r * perp_wall_angle.sin(),
                    },
                pos2: wall.pos2
                    - Dot {
                        x: wall.r * perp_wall_angle.cos(),
                        y: wall.r * perp_wall_angle.sin(),
                    },
                r: 0.0,
            },
        );
        let min_dst_to_line = (Dot::dst_to_line(ball.pos, wall_lines.0))
            .min(Dot::dst_to_line(ball.pos, wall_lines.1));
        let min_dst_to_endline =
            (Dot::dst(ball.pos, wall.pos1)).min(Dot::dst(ball.pos, wall.pos2)) - wall.r;

        if min_dst_to_line < min_dst_to_endline {
            let wall_angle = Dot::angle(wall.pos1, wall.pos2);
            ball.vel = Dot::rotate(
                Dot::null(),
                ball.vel,
                2.0 * std::f32::consts::PI - wall_angle,
            );
            ball.vel.y = -ball.vel.y;
            ball.vel = Dot::rotate(
                Dot::null(),
                ball.vel,
                wall_angle - 2.0 * std::f32::consts::PI,
            );

            return ball;
        }

        let check_endline = Ball::collision_resolution_ball(
            ball,
            Ball::new(wall.pos1.x, wall.pos1.y, 0.0, 0.0, wall.r, 10000000.0),
        );
        if check_endline.0 != ball {
            return check_endline.0;
        }

        Ball::collision_resolution_ball(
            ball,
            Ball::new(wall.pos2.x, wall.pos2.y, 0.0, 0.0, wall.r, 10000000.0),
        )
        .0
    }
    */

    pub fn time_to_intersect(b1: Ball, b2: Ball) -> Option<f32> {
        // pos = pos0 + V*t
        // dX^2 + dY^2 = (r1 + r2)^2

        let dvx = b2.vel.x - b1.vel.x;
        let dxo = b2.pos.x - b1.pos.x;

        let dvy = b2.vel.y - b1.vel.y;
        let dyo = b2.pos.y - b1.pos.y;

        let r = b1.r + b2.r;

        let t_option = solve_quadratic(
            dvx * dvx + dvy * dvy,
            2.0 * dvx * dxo + 2.0 * dvy * dyo,
            dxo * dxo + dyo * dyo - r * r,
        );

        if t_option.0.is_none() {
            return None;
        }
        if t_option.0.unwrap().min(t_option.1.unwrap()).is_nan() {
            return None;
        }
        if t_option.0.unwrap().min(t_option.1.unwrap()) < 0.0 {
            return None;
        }

        Some(t_option.0.unwrap().min(t_option.1.unwrap()))
    }
}
