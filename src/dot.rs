use crate::MyDrawToWindow;
use macroquad::prelude::*;

#[derive(Clone, Copy, PartialEq, Debug)]
pub struct Dot {
    pub x: f32,
    pub y: f32,
}

impl MyDrawToWindow for Dot {
    fn draw(&self) {
        draw_circle(
            self.x,
            self.y,
            screen_width().min(screen_height()) * 0.01,
            YELLOW,
        );
    }
}

impl std::ops::Add for Dot {
    type Output = Dot;

    fn add(self, other: Dot) -> Dot {
        Dot {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}
impl std::ops::Sub for Dot {
    type Output = Dot;

    fn sub(self, other: Dot) -> Dot {
        Dot {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}
impl std::ops::Mul for Dot {
    type Output = Dot;

    fn mul(self, other: Dot) -> Dot {
        Dot {
            x: self.x * other.x,
            y: self.y * other.y,
        }
    }
}

impl Dot {
    pub fn null() -> Dot {
        Dot { x: 0.0, y: 0.0 }
    }

    pub fn dst(d1: Dot, d2: Dot) -> f32 {
        let d3 = d1 - d2;
        (d3.x * d3.x + d3.y * d3.y).sqrt()
    }

    /*
    fn dst_to_line(d: Dot, source_line: Line) -> f32 {
        let mut line = source_line;
        let line_angle = Dot::angle(line.pos1, line.pos2);
        line.pos2 = Dot::rotate(
            line.pos1,
            line.pos2,
            2.0 * std::f32::consts::PI - line_angle,
        );
        let d = Dot::rotate(line.pos1, d, 2.0 * std::f32::consts::PI - line_angle);
        if d.x > line.pos2.x || d.x < line.pos1.x {
            return Dot::dst(d, line.pos1).min(Dot::dst(d, line.pos2));
        }

        (d.y - line.pos1.y).abs()
    }
    */

    pub fn angle(d1: Dot, d2: Dot) -> f32 {
        (d1.y - d2.y).atan2(d1.x - d2.x)
    }

    pub fn rotate(d1: Dot, d2: Dot, angle: f32) -> Dot {
        let cur_angle = Dot::angle(d1, d2);
        let next_angle = cur_angle + angle;
        let size = Dot::dst(d1, d2);
        Dot {
            x: next_angle.cos() * size + d1.x,
            y: next_angle.sin() * size + d1.y,
        }
    }

    pub fn from_tuple(tuple: (f32, f32)) -> Dot {
        Dot {
            x: tuple.0,
            y: tuple.1,
        }
    }

    pub fn from_num(num: f32) -> Dot {
        Dot { x: num, y: num }
    }
}
