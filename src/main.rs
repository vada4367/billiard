pub mod ball;
pub mod dot;
pub mod utils;

use ball::*;
use dot::*;
use utils::*;

use macroquad::prelude::*;
use std::time::{Duration, SystemTime};

#[macroquad::main("Billiard by Wagner")]
async fn main() {
    let mut balls: Vec<Ball> = vec![];
    //let mut walls: Vec<Line> = vec![];
    let mut ball_in_mouse: Option<usize> = None;
    //let mut start_line: Option<Dot> = None;
    let mut next_collide_time: Option<SystemTime> = None;

    loop {
        balls = update(
            input(balls.clone(), &mut ball_in_mouse),
            &mut next_collide_time,
        );
        render(&balls);

        next_frame().await;
        println!("{}", balls.len());
        println!("{:?}", next_collide_time);
    }
}

fn input(mut balls: Vec<Ball>, ball_in_mouse: &mut Option<usize>) -> Vec<Ball> {
    if is_mouse_button_pressed(MouseButton::Left) {
        for (i, ball) in balls.iter().enumerate() {
            if Dot::dst(ball.pos, Dot::from_tuple(mouse_position())) < ball.r {
                *ball_in_mouse = Some(i);
            }
        }
        if ball_in_mouse.is_none() {
            balls.push(Ball::new(
                mouse_position().0,
                mouse_position().1,
                0.0,
                0.0,
                screen_width().min(screen_height()) * 0.05,
                1.0,
            ));
        }
    }
    if is_mouse_button_released(MouseButton::Left) {
        if ball_in_mouse.is_some() && ball_in_mouse.unwrap() < balls.len() {
            let ball = &mut balls[ball_in_mouse.unwrap()];
            ball.vel = ball.pos - Dot::from_tuple(mouse_position());
        }
        *ball_in_mouse = None;
    }

    balls
}

fn update(mut balls: Vec<Ball>, next_collide_time: &mut Option<SystemTime>) -> Vec<Ball> {
    let mut first_intersect: Option<f32> = None;
    for i in 0..balls.len() {
        for j in (0..balls.len()).rev() {
            if let Some(time_to_intersect) = Ball::time_to_intersect(balls[i], balls[j]) {
                if first_intersect.is_none() || time_to_intersect < first_intersect.unwrap() {
                    first_intersect = Some(time_to_intersect);
                }
            }
        }
    }
    if let Some(time_to_intersect) = first_intersect {
        let option_intersect =
            SystemTime::now() + Duration::from_millis((time_to_intersect * 1000.0) as u64);
        if next_collide_time.is_none() || option_intersect < next_collide_time.unwrap() {
            *next_collide_time = Some(option_intersect);
        }
    }
    for ball in &mut balls {
        ball.pos = ball.pos + ball.vel * Dot::from_num(1.0 / 60.0);
    }
    if let Some(collide_time) = next_collide_time {
        if *collide_time < SystemTime::now() {
            for i in 0..balls.len() {
                for j in 0..balls.len() {
                    if j <= i {
                        continue;
                    }
                    (balls[i], balls[j]) = Ball::collision_resolution_ball(balls[i], balls[j]);
                }
            }
            *next_collide_time = None;
        }
    }
    for i in (0..balls.len()).rev() {
        let ball = balls[i];
        if ball.pos.x + ball.r < 0.0
            || ball.pos.x - ball.r > screen_width()
            || ball.pos.y + ball.r < 0.0
            || ball.pos.y - ball.r > screen_height()
        {
            balls.remove(i);
        }
    }

    balls
}

fn render(balls: &[Ball]) {
    clear_background(Color::new(28. / 255., 28. / 255., 28. / 255., 1.0));
    for ball in balls {
        ball.draw();
    }
}
