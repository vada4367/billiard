pub trait MyDrawToWindow {
    fn draw(&self);
}

pub fn solve_quadratic(a: f32, b: f32, c: f32) -> (Option<f32>, Option<f32>) {
    let d = b * b - 4.0 * a * c;
    if d < 0.0 {
        return (None, None);
    }

    (
        Some((d.sqrt() - b) * 0.5 / a),
        Some((-d.sqrt() - b) * 0.5 / a),
    )
}
